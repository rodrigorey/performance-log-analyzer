package com.tangomc.pla;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.tangomc.pla.agent.PLAgent;
import com.tririga.custom.integration.util.ClassLoaderUtils;
import com.tririga.custom.integration.util.JSONUtils;
import com.tririga.ws.TririgaWS;


public abstract class BaseController implements Controller {

	private static final Logger LOG = Logger.getLogger(BaseController.class);

	private boolean jsonResponse = true;
	
	protected TririgaWS client;
	
	private static PLAgent PLA_AGENT = null;
	
	public static PLAgent getAgent(){
		if(PLA_AGENT==null){
			PLA_AGENT = new PLAgent();
		}
		return PLA_AGENT;
	}
	
	/**
	 * @param objects
	 * @return
	 * @throws JSONException 
	 */
	public JSONObject anyNull(String[] objects) throws JSONException {
		JSONObject json = null;
		for(String s : objects){
			if(StringUtils.isBlank(s)){
				if(json==null){
					json = new JSONObject();
				}
				json.put(ERROR, "Missing parameter ["+s+"] not defined.");
			}
		}		
		return json;
	}
	
	/**
	 * @param response
	 * @throws JSONException 
	 * @throws IOException 
	 */
	public void returnSuccess(HttpServletResponse response) throws JSONException, IOException {
		JSONObject json = new JSONObject();
		json.put(STATUS, SUCCESS);
		JSONUtils.prettyPrint(response, json);
		return;
	}
	
	public String missingRequiredParameterMessage(Map<String, Object> objs){
		StringBuilder sb = new StringBuilder();		
		for(String s : objs.keySet()){
			sb.append(", ").append(s).append("[").append(objs.get(s)).append("]");
		}
		String msg = sb.toString();		
		//remove the ", " 		
		sb = new StringBuilder("A required parameter is missing: ").append(msg.substring(2));
		return sb.toString();
				
	}
	
	public static JSONObject handleError(String s) throws JSONException{
		JSONObject json = new JSONObject();
		json.put(ERROR, s);
		return json;
	}
	
	public String getHostNameUrl(HttpServletRequest request, boolean isDebug) throws UnknownHostException{
		String hostname = InetAddress.getLocalHost().getHostName();
		return getUrl(hostname, request, isDebug);
	}
	
	/**
	 * <p>This method will build out the url with the ip address or hostname passed in.</p>
	 * @param host either ip address or hostname
	 * @param request
	 * @param isDebug
	 * @return String
	 * @throws UnknownHostException 
	 */
	public String getUrl(String host, HttpServletRequest request, boolean isDebug) throws UnknownHostException{
		String scheme = request.getScheme();
		int port = request.getServerPort();
		String uri = request.getRequestURI();
		StringBuilder sb = new StringBuilder();
		sb.append(scheme).append("://").append(host).append(":").append(port).append(uri);
		String url = sb.toString();
		ClassLoaderUtils.Log("constructed url ["+url+"] ", LOG, isDebug);
		return url;
	}	
	
	public boolean isJsonResponse(){
		return jsonResponse;
	}
	
	public void setJsonResponse(boolean jsonResponse){
		this.jsonResponse = jsonResponse;
	}

	
	//attributes used in req/resp
	public static final String ATTR_RECORD_ID = "recordId";
	public static final String ATTR_MODULE = "module";
	public static final String ATTR_BO = "bo";
	public static final String ATTR_FORM = "form";
	public static final String ATTR_MODULES = "modules";
	public static final String ATTR_NAME = "name";
	public static final String ATTR_ID = "id";
	public static final String ATTR_BOS = "bos";
	public static final String ATTR_ASSOC_RECORD_ID = "associatedRecordId";
	public static final String ATTR_ASSOC_NAME = "association";
	public static final String ATTR_ACTION_NAME = "actionName";
	public static final String ATTR_STATUS = "status";
	
	//json respose params
	public static final String ERROR 	= "error";
	public static final String STATUS 	= "status";
	public static final String SUCCESS 	= "success";
}
