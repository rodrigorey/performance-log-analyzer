package com.tangomc.pla;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tririga.ws.TririgaWS;

public interface Controller {
	
	/**
	 * 
	 * @param client
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public abstract void run(TririgaWS client, HttpServletRequest request, HttpServletResponse response) throws Exception;
}
