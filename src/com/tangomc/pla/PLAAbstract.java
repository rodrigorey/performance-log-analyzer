/** 
 *
 */
package com.tangomc.pla;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

import com.tangomc.pla.client.Settings;
import com.tangomc.pla.utils.HttpUtils;
import com.tririga.custom.integration.service.dao.QueryService;
import com.tririga.custom.integration.service.query.QueryFilter;
import com.tririga.custom.integration.service.query.TririgaQuery;
import com.tririga.custom.integration.service.query.TririgaResultSet;
import com.tririga.ws.TririgaWS;

/**
 * 
 */
public abstract class PLAAbstract {

	public TririgaWS client;
	
	public abstract void process(Long recordId) throws Exception;
	
	/**
	 * TODO: figure out how to get the server context.
	 * Logs into the admin console and returns the jsessionid instantiated.  
	 * @param settings
	 * @throws IOException
	 */
	public String loginAdminConsole(Settings settings) throws IOException{
		
		String url = "";
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("objectId", 1000);
		params.put("actionId", 1002);
		params.put("USERNAME", settings.getUser());
		params.put("PASSWORD", settings.getPass());
		HttpResponse response = HttpUtils.postUrl(url, params, null);
		
		//admin login does a 302 redirect with jsessionid valid, so just pull that, ignore the rest.
		StatusLine status = response.getStatusLine();
		if(status.getStatusCode()==302){
			//we good
			Header[] headers = response.getAllHeaders();
			for(Header h : headers){
				if(HttpUtils.JSESSIONID.equalsIgnoreCase(h.getName())){
					return h.getValue();
				}
			}
		} else {
			if(status.getStatusCode()>=400){
				throw new IOException("Could not login to server: "+status.getStatusCode()+" - "+status.getReasonPhrase());
			}
		}
		return null;
	}
	
	/**
	 * queries tririga and gets the settings object representing the tmcPerformance object
	 * @param recordId
	 * @return
	 * @throws Exception
	 */
	public Settings getSettings(Long recordId) throws Exception{
		TririgaQuery query = buildQueryForSettings(recordId);
		TririgaResultSet trs = runQuery(query);
		return new Settings(trs);
	}
	
	/**
	 * Builds a query object ready for execute with a filter by recordId using the 
	 * triRecordIdSY field.
	 * @param recordId
	 * @return populated query object
	 */
	public TririgaQuery buildQueryForSettings(Long recordId){
		TririgaQuery query = new TririgaQuery();
		query.setDynamic(false);
		query.setModuleName("tmcPerformance");
		query.setBoNames(new String[]{"tmcPerformance"});
		query.setQueryName("tmcPerformance - json request");
		//add the filter
		QueryFilter filter = new QueryFilter("General", "triRecordIdSY", String.valueOf(recordId));
		query.addFilter(filter);
		return query;
	}
	
	public TririgaResultSet runQuery(TririgaQuery query) throws Exception{
		QueryService service = new QueryService(client, true);
		return service.queryForRecords("Fetching query["+query.getQueryName()+"]", query);
	}
	
}
