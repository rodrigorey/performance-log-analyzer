/** 
 *
 */
package com.tangomc.pla.agent;

import java.io.File;
import java.util.Date;

import javax.mail.internet.MimeMessage;

import org.apache.commons.json.JSONObject;
import org.apache.log4j.Logger;

import com.tangomc.pla.services.ProcessLogs;
import com.tangomc.pla.utils.SystemUtils;


/**
 * 
 */
public class PLAgent extends Thread {
	
	private static final Logger LOG = Logger.getLogger(PLAgent.class);
	
	private volatile boolean running = false;
	private volatile String dir="", server="";
	
	private int sleepTime = 1000; //1 second
	private long endtime = 0;

	//exposed for testing
	protected boolean logsEnabled = false; 
	
	public PLAgent(){
		start();
	}
	
	public void setEndtime(long endtime){
		this.endtime = endtime;
	}
	public long getEndtime(){
		return this.endtime;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run(){
		while(true){
			if(isRunning()){
				if(endTimeReached()){
					//process logs and kill logging
					try{
						processLogs();
					} catch(Exception e){
						LOG.error("Could not process logs to send out["+e.getMessage()+"]", e);
					} 
					killLogs();
				} else {
					//check that logs are enabled
					checkLogs();
				}
			}
			try {
				Thread.sleep(this.sleepTime);
			} catch (InterruptedException e) {
				LOG.error(e.getMessage(),e);
			}	
		}
	}
	
	/**
	 * @throws Exception 
	 * 
	 */
	private void processLogs() throws Exception {
		ProcessLogs pl = new ProcessLogs();
		JSONObject json = SystemUtils.getLinksToLatestFiles(this.dir);
		String fileNames = String.format("%s,%s", json.getString("minmax"), json.getString("summary"));
		File zip = pl.go(dir, fileNames);
		MimeMessage message = pl.createNotificationForPerformance(zip, server);
		pl.sendMessage(message);
		return;
	}

	/**
	 * check is logging is turned on or not and if not, turn it on.
	 */
	protected void checkLogs() {
		if(logsEnabled){
			return;
		}		
		logsEnabled = true;
	}

	/**
	 * turn off logging.
	 */
	protected void killLogs() {
		stopRunning();
		logsEnabled = false;
	}

	/**
	 * @return
	 */
	private boolean endTimeReached() {
		return (System.currentTimeMillis()>=endtime);
	}
	
	/**
	 * 
	 */
	public synchronized void stopRunning() {	
		LOG.info("PLA Agent shutting down...");
		this.running = false;
	}

	/**
	 * 
	 * @param endtime - time when to stop loggings and send out the analysis
	 * @param dir - base logs directory
	 * @param server - originating server ip address.
	 */
	public synchronized void startRunning(long endtime,String dir, String server) {		
		LOG.info("PLA Agent starting... scheduled to end on "+new Date(endtime));
		this.dir = dir;
		this.server = server;
		this.endtime = endtime;
		this.running = true;
	}

	/**
	 * @return
	 */
	public synchronized boolean isRunning() {
		return running;
	}
}
