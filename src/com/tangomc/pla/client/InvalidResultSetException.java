/** 
 *
 */
package com.tangomc.pla.client;

/**
 * 
 */
public class InvalidResultSetException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4566948100470827816L;

	/**
	 * 
	 */
	public InvalidResultSetException() {
	}

	/**
	 * @param paramString
	 */
	public InvalidResultSetException(String paramString) {
		super(paramString);
	}

	/**
	 * @param paramThrowable
	 */
	public InvalidResultSetException(Throwable paramThrowable) {
		super(paramThrowable);
	}

	/**
	 * @param paramString
	 * @param paramThrowable
	 */
	public InvalidResultSetException(String paramString,
			Throwable paramThrowable) {
		super(paramString, paramThrowable);
	}

}
