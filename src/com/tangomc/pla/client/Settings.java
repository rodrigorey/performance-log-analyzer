/** 
 *
 */
package com.tangomc.pla.client;

import org.apache.commons.lang.StringUtils;

import com.tririga.custom.integration.service.query.TririgaResultSet;
import com.tririga.custom.integration.service.query.TririgaRow;

/**
 * 
 */
public class Settings {

	private String logPath, user, pass;
	private Integer hrsOfMagic = 8;
	
	/**
	 * 
	 */
	public Settings(String logPath, String user, String pass) {
		this.logPath = logPath;
		this.user = user;
		this.pass = pass;
	}

	/**
	 * pass the row with the config settings in a tririga row object to populate this object.
	 * @param trs
	 * @throws Exception 
	 */
	public Settings(TririgaResultSet trs) throws Exception{
		if(trs==null || trs.getTotalResults()==0){
			throw new InvalidResultSetException("Could not find result for settings query.");
		}
		//should always be one
		trs.next();
		TririgaRow row = trs.getTririgaRow();
		this.logPath = row.getValue("path");
		this.user = row.getValue("user");
		this.pass = row.getValue("pass");
		String hours = row.getValue("hours");
		if(StringUtils.isNotBlank(hours)){
			this.hrsOfMagic = Integer.parseInt(hours);
		}
	}
	
	/**
	 * @return the logPath
	 */
	public String getLogPath() {
		return logPath;
	}

	/**
	 * @param logPath the logPath to set
	 */
	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * @param pass the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}

	/**
	 * @return the hrsOfMagic
	 */
	public Integer getHrsOfMagic() {
		return hrsOfMagic;
	}

	/**
	 * @param hrsOfMagic the hrsOfMagic to set
	 */
	public void setHrsOfMagic(Integer hrsOfMagic) {
		this.hrsOfMagic = hrsOfMagic;
	}
		
}
