/** 
 *
 */
package com.tangomc.pla.controllers;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.tangomc.pla.BaseController;
import com.tririga.custom.integration.util.JSONUtils;
import com.tririga.ws.TririgaWS;

/**
 * deletes the performance analysis logs in the output folder
 */
public class CleanLogs extends BaseController {

	private static final Logger LOG = Logger.getLogger(CleanLogs.class);
	
	/* (non-Javadoc)
	 * @see com.tangomc.pla.Service#run(com.tririga.ws.TririgaWS, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void run(TririgaWS client, HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject json = new JSONObject();
		try{
			String dir = request.getParameter("dir");
			if(StringUtils.isBlank(dir)){
				throw new MissingArgumentException("Missing parameter [dir] in request. Cannot process logs.");
			}
			clean(dir);
			json.put("status", "success");
		} catch(Exception e){
			json = BaseController.handleError(e.getMessage());
		}
		JSONUtils.prettyPrint(response, json);
		return;
	}

	/**
	 * @param dir
	 */
	public void clean(String dir) {
		File outputDir = new File(dir+File.separator+"output");
		File[] files = outputDir.listFiles();
		for(File f : files){
			LOG.info("deleting ["+f.getName()+"]");
			f.delete();
		}
	}

}
