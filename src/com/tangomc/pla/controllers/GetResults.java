/** 
 *
 */
package com.tangomc.pla.controllers;

import java.io.File;
import java.io.IOException;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.apache.log4j.Logger;

import com.tangomc.pla.BaseController;
import com.tangomc.pla.services.ProcessLogs;
import com.tangomc.pla.utils.ResponseUtil;
import com.tririga.custom.integration.util.JSONUtils;
import com.tririga.custom.integration.util.ReqUtils;
import com.tririga.ws.TririgaWS;

/**
 * Service is a call to download files
 */
public class GetResults extends BaseController {

	private static final Logger LOG = Logger.getLogger(GetResults.class);
	
	/* (non-Javadoc)
	 * @see com.tangomc.pla.Service#run(com.tririga.ws.TririgaWS, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void run(TririgaWS client, HttpServletRequest request, HttpServletResponse response) throws IOException, JSONException {
		this.client = client;
		String dir = request.getParameter("dir"); 
		String fileNames = request.getParameter("files"); 
		boolean isDownload = ReqUtils.getBoolean(request.getParameter("dl"), true);
		
		JSONObject error = anyNull(new String[]{dir, fileNames});
		if(error!=null){
			JSONUtils.print(response, error);
			return;
		}
		
		try{			
			ProcessLogs process = new ProcessLogs();
			File zip = process.go(dir, fileNames);
			
			if(isDownload){
				ResponseUtil.promptForDownload(response, zip);
			} else {
				//email this out to tango mc email bot.
				MimeMessage message = process.createNotificationForPerformance(zip, request.getRemoteHost());
				process.sendMessage(message);
			}
			
		} catch(Exception e){
			LOG.error(e,e);
			JSONUtils.print(response, BaseController.handleError(e.getMessage()));
		}
		return;
	}
}
