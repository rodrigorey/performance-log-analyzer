/** 
 *
 */
package com.tangomc.pla.controllers;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.json.JSONArray;
import org.apache.commons.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.tangomc.pla.BaseController;
import com.tangomc.pla.utils.ResponseUtil;
import com.tangomc.pla.utils.SystemUtils;
import com.tangomc.tpr.TprController;
import com.tangomc.tpr.processor.Observer;
import com.tangomc.tpr.processor.summary.SummaryData;
import com.tangomc.tpr.processor.summary.SummaryProcessor;
import com.tririga.ws.TririgaWS;

/**
 * 
 */
public class PerfLog extends BaseController {

	private static final Logger LOG = Logger.getLogger(PerfLog.class);
	
	private String format = null;
	
	/*
	 * (non-Javadoc)
	 * @see com.tangomc.pla.Service#run(com.tririga.ws.TririgaWS, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void run(TririgaWS client, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		JSONObject json = new JSONObject();
		format = request.getParameter("f");
		try{
			String dir = request.getParameter("dir");
			if(StringUtils.isBlank(dir)){
				throw new MissingArgumentException("Missing parameter [dir] in request. Cannot process logs.");
			}
			Collection<SummaryData> summaryData = getSummaryData(dir);
			
			json.put("header", SummaryData.HEADER);
			JSONArray jsonSds = new JSONArray();
			for(SummaryData sd : summaryData){
				JSONObject jsonSd = new JSONObject();
				//"type", "key", "minVal", "maxVal","totalDuration", "totalCount"
				jsonSd.put("type", sd.getType());
				jsonSd.put("key", sd.getKey());
				jsonSd.put("minVal", sd.getMinVal());
				jsonSd.put("maxVal", sd.getMaxVal());
				jsonSd.put("totalDuration", sd.getTotalDuration());
				jsonSd.put("totalCount", sd.getTotalCount());
				jsonSds.add(jsonSd);
			}
			json.put("data", jsonSds);
			
			JSONObject links = SystemUtils.getLinksToLatestFiles(dir);
			json.put("links", links);
			
		} catch(Exception e){
			LOG.error(e,e);
			json = BaseController.handleError(e.getMessage());
		}
		ResponseUtil.render(response, json, format);		
		return;
	}

	/**
	 * @param dir
	 * @return
	 * @throws Exception 
	 */
	public Collection<SummaryData> getSummaryData(String dir) throws Exception {
		TprController tpr = new TprController(dir);
		//collects the data and writes to disk
		tpr.run();
		
		//grab the data to display to the user
		List<Observer> obs = tpr.getObservers();
		Collection<SummaryData> summaryData = null;
		for(Observer o : obs){
			if(o instanceof SummaryProcessor){ //dont' care about other types
				summaryData = o.getSummary();
				break;
			}				
		}
		return summaryData;
	}
}
