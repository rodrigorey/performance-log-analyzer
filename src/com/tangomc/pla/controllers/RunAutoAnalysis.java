/** 
 *
 */
package com.tangomc.pla.controllers;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.tangomc.pla.BaseController;
import com.tririga.custom.integration.util.JSONUtils;
import com.tririga.custom.integration.util.ReqUtils;
import com.tririga.pub.adapter.IConnect;
import com.tririga.ws.TririgaWS;

/**
 * Automated listener class to turn on logging, analyze the logs, then zip up files and 
 * email them out. 
 * 
 * TODO: need to wrap the email into the workflow process.
 */
public class RunAutoAnalysis extends BaseController {

	public static final Logger LOG = Logger.getLogger(RunAutoAnalysis.class);
		
	private long defaultEndtime = 60000; //1 min for testing
			
	/* (non-Javadoc)
	 * @see com.tangomc.pla.Service#run(com.tririga.ws.TririgaWS, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void run(TririgaWS client, HttpServletRequest request, HttpServletResponse response) throws Exception {
		this.client = client;
		boolean status = ReqUtils.getBoolean(request.getParameter("s"), false); 
		if(status){
			checkStatus(response, status);
			return;
		}
		
		if(getAgent().isRunning()){
			//check for force quit parameter
			if(ReqUtils.getBoolean(request.getParameter("k"), false)){ //k for kill
				//TODO: eventually need to check that it's a post and not just a get					
				String s = String.format("Force quit initialized from ip: [%s] and user: [%s].", request.getRemoteHost(), request.getAttribute(IConnect.ATTR_UNAME));
				LOG.info(s);
				stopAgent(response);
			} else {
				alreadyRunning(response);
			}
		} else {
			//collect end date from request and start			
			startAgent(request, response);
		}
		return;
	}

	/**
	 * @param response
	 * @param status
	 * @throws JSONException 
	 * @throws IOException 
	 */
	private void checkStatus(HttpServletResponse response, boolean status) throws IOException, JSONException {
		JSONObject json = new JSONObject();
		json.put(STATUS, "Currently "+(getAgent().isRunning()?" Running":" NOT Running"));
		JSONUtils.prettyPrint(response, json);
	}

	/**
	 * @param request 
	 * @param response
	 * @throws IOException 
	 * @throws JSONException 
	 */
	@SuppressWarnings("unchecked")
	private void startAgent(HttpServletRequest request, HttpServletResponse response) throws JSONException, IOException {
		
		JSONObject json = new JSONObject();
		
		String endtime = request.getParameter("time");
		long et = defaultEndtime;
		if(StringUtils.isBlank(endtime)){				
			json = handleError(String.format("Could not determine parameter [time] in request, using default end time of %s", et));
			JSONUtils.prettyPrint(response, json);
			return;
		} else {
			//multiply for the hour in millis and if take only positive values.
			et = Math.abs(Long.parseLong(endtime)) * 3600000;
		}
		//now add the current time to endtime to get the real time in the future from now.
		et += System.currentTimeMillis();
		
		String dir = request.getParameter("dir");  
		JSONObject jo = anyNull(new String[]{dir});
		if(jo!=null){
			json.putAll(jo);
			JSONUtils.prettyPrint(response, json);
			return;
		}
			
		getAgent().startRunning(et, dir, request.getRemoteHost());
		returnSuccess(response);
		return;
	}

	/**
	 * @param response
	 * @throws JSONException 
	 * @throws IOException 
	 */
	private void stopAgent(HttpServletResponse response) throws JSONException, IOException {
		getAgent().stopRunning();
		returnSuccess(response);
		return;
	}

	/**
	 * @param response
	 * @return
	 * @throws JSONException 
	 * @throws IOException 
	 */
	private void alreadyRunning(HttpServletResponse response) throws IOException, JSONException {
		JSONObject json = new JSONObject();
		json.put(STATUS, "Analysis is currently running with expected end date of "+new Date(getAgent().getEndtime())+"]");
		JSONUtils.prettyPrint(response, json);
		return;
	}
	
}
