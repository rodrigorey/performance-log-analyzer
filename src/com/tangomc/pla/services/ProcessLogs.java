/** 
 *
 */
package com.tangomc.pla.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * handles controlling the processing of the logs.
 */
public class ProcessLogs {
	
	public static final Logger LOG = Logger.getLogger(ProcessLogs.class);
	
	public ProcessLogs(){}
	
	/**
	 * Returns a zip file of the all the system logs.
	 * @return
	 * @throws IOException 
	 */
	public File go(String dir, String fileNames) throws IOException {
		List<File> files = packageFiles(dir, fileNames);			
		File zip = zip(files);
		return zip;
	}
	
	private List<File> packageFiles(String dir, String fileNames) throws IOException{
		File outputDir = new File(dir+File.separator+"output");
		String[] fileNamesAsArray = fileNames.split(",");
		List<File> files = new ArrayList<File>();
		for(String s : fileNamesAsArray){
			File f = new File(outputDir+File.separator+s);
			if(!f.exists()){
				throw new IOException("File ["+f.getAbsolutePath()+"] does not exist.");
			}
			files.add(f);
		}
		files.add(collectProperties(dir));
		
		return files;
	}

	/**
	 * @return
	 * @throws IOException 
	 */
	protected File collectProperties(String dir) throws IOException {
		File f = new File("System.properties");
		f.deleteOnExit();
		FileOutputStream fos = new FileOutputStream(f);
		try{
			try{
				Map<String, String> env = System.getenv();
				fos.write("#System environment variables \n".getBytes());
				for(String key : env.keySet()){
					fos.write(String.format("%s=%s\n", key, env.get(key)).getBytes());
				}
			} catch(Exception e){
				//ignore, won't always get these back
			}
			
			fos.write("\n#System properties \n".getBytes());
			Properties props = System.getProperties();
			for(Object key : props.keySet()){
				fos.write(String.format("%s=%s\n", key, props.get(key)).getBytes());
			}
			
			fos.write("\n#Runtime properties \n".getBytes());
			fos.write(String.format("Processors=%s\n", Runtime.getRuntime().availableProcessors()).getBytes());
			fos.write(String.format("Free Memory=%s\n", Runtime.getRuntime().freeMemory()).getBytes());
			fos.write(String.format("Max Memory=%s\n", Runtime.getRuntime().maxMemory()).getBytes());
			fos.write(String.format("Total Memory=%s\n", Runtime.getRuntime().totalMemory()).getBytes());		
			
			fos.write("\n#File root properties \n".getBytes());
		    File[] roots = File.listRoots();
		    for (File root : roots) {
		    	fos.write(String.format("\nFile system root=%s\n", root.getAbsolutePath()).getBytes());
		    	fos.write(String.format("Total space=%s\n",root.getTotalSpace()).getBytes());
		    	fos.write(String.format("Free space=%s\n",root.getFreeSpace()).getBytes());
		    	fos.write(String.format("Usable space=%s\n",root.getUsableSpace()).getBytes());
		    }
		    
		} finally{
			IOUtils.closeQuietly(fos);
		}
		
		return collectPropertiesFiles(dir, f);
		
	}

	/**
	 * @param dir
	 * @return
	 * @throws IOException 
	 */
	private File collectPropertiesFiles(String dir, File mainProps) throws IOException {
		if(StringUtils.isBlank(dir)){
			throw new IOException("parameter [dir] cannot be empty. Please specify the directory for the logs.");
		}
		
		//attempt to extract the value log from the directory
		try{
			dir = dir.substring(0, dir.indexOf("log"));
		} catch(Exception e){
			//ignore
		}
		String path = dir + File.separator + "config" + File.separator;
		File configDir = new File(path);
		if(configDir.exists() && configDir.isDirectory()){			
		    PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(mainProps, true)));
		    try{
			    Properties props = null;
				File[] list = configDir.listFiles();
				for(File f : list){
					if(f.isFile() && f.getName().endsWith(".properties")){
						props = new Properties();
						props.load(new FileInputStream(f));
						pw.write("\n#"+f.getName()+"\n");					
						for(Object key : props.keySet()){
							pw.write(String.format("%s=%s\n", key, props.get(key)));
						}
					}
				}
		    } finally {
		    	IOUtils.closeQuietly(pw);
		    }
		}		
		return mainProps;
	}

	/**
	 * Takes the file passed in and creates a notification in tririga
	 * @param zip
	 * @param response
	 * @return 
	 */
	public MimeMessage createNotificationForPerformance(File zip, String server) throws Exception {
	      
		//add new ones with comma, no space
		String to 	= "rodrigo.rey@tangomc.com,bill.thornton@tangomc.com";
	      
	    String from = "performance@tangomc.com";
		String host = "localhost";

		Properties properties = System.getProperties();
		properties.setProperty("mail.smtp.host", host);
		Session session = Session.getDefaultInstance(properties);

		MimeMessage message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));

		String[] recips = to.split(",");
		List<Address> addressees = new ArrayList<Address>();
		for(String s : recips){
			addressees.add(new InternetAddress(s));
		}
		message.addRecipients(Message.RecipientType.TO, (Address[])addressees.toArray(new Address[addressees.size()]));
		message.setSubject("PLA from " + server);

		BodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setText("");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		// Part two is attachment
		messageBodyPart = new MimeBodyPart();
		String filename = zip.getName();
		DataSource source = new FileDataSource(zip);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(filename);
		multipart.addBodyPart(messageBodyPart);

		message.setContent(multipart);
		return message;
	}

	/**
	 * @param message
	 * @throws MessagingException 
	 */
	public void sendMessage(MimeMessage message) throws MessagingException {
		Transport.send(message);
		LOG.info("Sent message successfully....");
	}

	/**
	 * Zip up the array of files passed in and return the zip
	 * @param files
	 * @return
	 * @throws IOException
	 */
	public static File zip(List<File> files) throws IOException {	
		File returnFile = new File("tangoMC_PLA.zip");
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(returnFile));
		try {
			for(File file : files){
				ZipEntry entry = new ZipEntry(file.getName());
				zos.putNextEntry(entry);

				FileInputStream fis = null;
				try {
					fis = new FileInputStream(file);
					byte[] byteBuffer = new byte[4096];
					int bytesRead = -1;
					while ((bytesRead = fis.read(byteBuffer)) != -1) {
						zos.write(byteBuffer, 0, bytesRead);
					}
					zos.flush();
				} finally {
					IOUtils.closeQuietly(fis);
				}				
			}
			zos.closeEntry();
			zos.flush();
		} finally {
			IOUtils.closeQuietly(zos);
		}
		return returnFile;
	}
}
