/** 
 *
 */
package com.tangomc.pla.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;


/**
 * 
 */
public class HttpUtils {
	
	public static final String USER_AGENT = "User-Agent";
	public static final String USER_AGENT_VALUE = "TMC v0.1";
	public static final String JSESSIONID = "JSESSIONID";
	
	private static Logger LOG = Logger.getLogger(HttpUtils.class);
	
	public static HttpResponse postUrl(String url, Map<String, Object> params, String cookieForAuth) throws IOException {
		
		HttpClient client = new DefaultHttpClient();
		try{
			HttpPost post = new HttpPost(url);
			
			post.addHeader(USER_AGENT, USER_AGENT_VALUE);
			
			if(StringUtils.isNotBlank(cookieForAuth)){
				post.addHeader(JSESSIONID, cookieForAuth);
			}
			
			List <NameValuePair> nvps = new ArrayList <NameValuePair>();
			for(String key : params.keySet()){
				nvps.add(new BasicNameValuePair(key, String.valueOf(params.get(key))));
			}	
	        post.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
	        
	        HttpResponse response =  client.execute(post);
	        return response;
		} finally {
			client.getConnectionManager().shutdown();
		}
	}
	
	public static String handleResponse(HttpResponse response) throws ParseException, IOException{
		StatusLine status = response.getStatusLine();
		Integer code = status.getStatusCode();
		if (code < 200 || code >= 400) {
			throw new RuntimeException("Unacceptable HTTP response: "+ code+", "+status.getReasonPhrase());
		}

		HttpEntity entity = response.getEntity();
		if(entity!=null){
    		String str = EntityUtils.toString(entity, HTTP.UTF_8);
    		return str;
		}
		throw new IOException("Did not get any body response. "); 
	}
	
	public static String postUrlSimple(String url, Map<String, Object> params, String cookieForAuth) throws IOException{
		LOG.info("Making post request to URL["+url+"]");
		URL urlConn = new URL(url);
		
		HttpURLConnection conn = (HttpURLConnection)urlConn.openConnection();
		HttpURLConnection.setFollowRedirects(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		
		for(String key : params.keySet()){
			conn.addRequestProperty(key, String.valueOf(params.get(key)));	
		}
		return processResponseFromURL(conn, cookieForAuth); 
	}
	
	
	public static String fetchUrl(String url, String cookieForAuth) throws IOException {
		LOG.info("Making connection to URL["+url+"]");
		URL urlConn = new URL(url);
		
		HttpURLConnection conn = (HttpURLConnection)urlConn.openConnection();
		HttpURLConnection.setFollowRedirects(true);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		
		return processResponseFromURL(conn, cookieForAuth);
	}
	
	private static String processResponseFromURL(HttpURLConnection conn, String cookieForAuth) throws IOException{
		if(StringUtils.isNotBlank(cookieForAuth)){
			conn.setRequestProperty("Cookie", cookieForAuth);
		}		
		Integer code = conn.getResponseCode();
		if (code < 200 || code >= 400) {
			throw new RuntimeException("Unacceptable HTTP response: "+ code+", "+conn.getResponseMessage());
		}
		HttpURLConnection.setFollowRedirects(true);
		StringBuilder sb = new StringBuilder();
		try{
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;			
			while ((output = br.readLine()) != null) {
				//reading stream
				sb.append(output);
			}
		} finally {
			conn.disconnect();
		}
		String response = sb.toString();
		LOG.info("Response: "+response);
		return response;
	}
}
