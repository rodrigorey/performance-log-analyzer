/** 
 *
 */
package com.tangomc.pla.utils;

/**
 * 
 */
public class InvalidRecordIdException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3046305806514250979L;

	/**
	 * 
	 */
	public InvalidRecordIdException() {
	}

	/**
	 * @param paramString
	 */
	public InvalidRecordIdException(String paramString) {
		super(paramString);
	}

	/**
	 * @param paramThrowable
	 */
	public InvalidRecordIdException(Throwable paramThrowable) {
		super(paramThrowable);
	}

	/**
	 * @param paramString
	 * @param paramThrowable
	 */
	public InvalidRecordIdException(String paramString, Throwable paramThrowable) {
		super(paramString, paramThrowable);
	}

}
