/** 
 *
 */
package com.tangomc.pla.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;
import org.apache.commons.lang.StringUtils;

import com.tririga.custom.integration.io.Query;
import com.tririga.custom.integration.util.JSONUtils;
import com.tririga.custom.integration.util.ResponseUtils;

/**
 * 
 */
public class ResponseUtil {
	
	public static void render(HttpServletResponse response, JSONObject json, String format) throws IOException, JSONException {
		if(Query.FORMAT_PJSON.equalsIgnoreCase(format) || StringUtils.isBlank(format)){			
			JSONUtils.prettyPrint(response, json); 
		} else if(Query.FORMAT_JSON.equalsIgnoreCase(format)){
			JSONUtils.print(response, json);
		} 
		return;		
	}
	
	public static void render(HttpServletRequest request, HttpServletResponse response, String classLoader) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append(ResponseUtils.getResourceURL(classLoader, request))
				.append("index.htm");		
		response.sendRedirect(sb.toString());
	}
	
	public static void promptForDownload(HttpServletResponse response, File file) throws IOException{
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-disposition","attachment; filename="+file.getName());

		OutputStream out = response.getOutputStream();
		FileInputStream in = new FileInputStream(file);
		try{	
			byte[] buffer = new byte[4096];
			int length;
			while ((length = in.read(buffer)) > 0){
			    out.write(buffer, 0, length);
			}
			out.flush();
		} finally {
			IOUtils.closeQuietly(in);
		}	
	}
}
