/** 
 *
 */
package com.tangomc.pla.utils;

import java.io.File;

import org.apache.commons.json.JSONException;
import org.apache.commons.json.JSONObject;

/**
 * 
 */
public class SystemUtils {
	
	public static String[] getLinksToLatestFilesAsArray(String dir) {		
		File outputDir = new File(dir+File.separator+"output");
		File[] files = outputDir.listFiles();
		long summaryLastModified = 0;
		long minmaxLastModified = 0;
		String summaryFileName="", minmaxFileName="";
		for(File f : files){
			if(f.getName().toLowerCase().startsWith("summary")){
				if(f.lastModified() > summaryLastModified){
					summaryLastModified = f.lastModified();							
					summaryFileName = f.getName();
				}
			}
			if(f.getName().toLowerCase().startsWith("minmax")){
				if(f.lastModified() > minmaxLastModified){
					minmaxLastModified = f.lastModified();							
					minmaxFileName = f.getName();
				}
			}
		}
		return new String[]{summaryFileName, minmaxFileName};
	}

	
	/**
	 * looks for the two most recent analysis files in the output/log dir
	 * @param dir
	 * @return
	 * @throws JSONException
	 */
	public static JSONObject getLinksToLatestFiles(String dir) throws JSONException{
		String[] fileNames = getLinksToLatestFilesAsArray(dir);		
		JSONObject json = new JSONObject();
		json.put("summary", fileNames[0]);
		json.put("minmax", fileNames[1]);
		return json;
	}
	
}
