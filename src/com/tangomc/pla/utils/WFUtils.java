/** 
 *
 */
package com.tangomc.pla.utils;

import org.apache.log4j.Logger;

import com.tririga.pub.workflow.Record;

/**
 * 
 */
public class WFUtils {
	
	private static final Logger LOG = Logger.getLogger(WFUtils.class);
	
	public static Long getRecordId(Record[] records) throws InvalidRecordIdException{
		Long recordId = -1L;
		try{
			recordId = records[0].getRecordId();			
			return recordId; 
		} catch(Exception e){
			throw new InvalidRecordIdException("Could not get recordId. "+e, e);
		}	
	}

}
