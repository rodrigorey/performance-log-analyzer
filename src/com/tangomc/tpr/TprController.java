package com.tangomc.tpr;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import com.tangomc.tpr.processor.Observer;
import com.tangomc.tpr.processor.summary.MinMaxFileStore;
import com.tangomc.tpr.processor.summary.SummaryFileStore;
import com.tangomc.tpr.processor.summary.SummaryProcessor;
import com.tangomc.tpr.reader.TprFileReader;

// TODO: Auto-generated Javadoc
/**
 * The Class TprController.
 */
public class TprController {
	
	private static final Logger LOG = Logger.getLogger(TprController.class);
	
	/** The dir name. */
	private String dirName = "";
	
	/** The file names. */
	private List<String> fileNames = new ArrayList<String>();

	/** The process observers. */
	private List<Observer> processObservers = new ArrayList<Observer>();
	
	private	Options options = new Options();
	
	private CommandLine cmd = null;

	private String[] acceptedPrefix = new String[]{"performance","systemmetrics"};
	
	// Set up parms for Processors and Stores
	private	SummaryProcessor sumProc = null;
	

	/**
	 * Instantiates a new tpr controller.
	 * 
	 * @param args
	 *            the args
	 * @throws Exception
	 *             the exception
	 */
	public TprController(String[] args) throws Exception {
		
		cmd = createOptions(args);
		
		setDirName(cmd);
		if (cmd.hasOption("h")) {
			printHelp();
			return;
		}
		setupParams();
	}
	
	/**
	 * This controller should be called from an embedded system
	 * @param dirName
	 */
	public TprController(String dirName){
		setDirName(dirName);
		setupParams();
	} 
	
	private void setupParams(){
		sumProc = new SummaryProcessor();
	
		long currentTime = System.currentTimeMillis();

		// Write to File
		SummaryFileStore fileStore = new SummaryFileStore();
		fileStore.setOutputFileName("Summary_"
				+ currentTime + ".csv");
		sumProc.addStore(fileStore);

		// Write Min Max Values to File
		MinMaxFileStore minMaxFileStore = new MinMaxFileStore();
		minMaxFileStore.setOutputFileName("MinMax_"
				+ currentTime + ".csv");
		sumProc.addStore(minMaxFileStore);		
		
		// Add Processor
		addObserver(sumProc);

	}
	
	/**
	 * Run.
	 *
	 * @throws Exception the exception
	 */
	public void run() throws Exception {
		TprFileReader tprFileReader = new TprFileReader();
		String fullPath = null;
		for(String fileName : fileNames) {
			fullPath = dirName+File.separator+fileName;
			log(fullPath);
			tprFileReader.setFileName(fullPath);
			tprFileReader.process();		
		}
		tprFileReader.store(this.dirName);

	}

	/**
	 * Sets the dir name.
	 *
	 * @param cmd the new dir name
	 */
	private void setDirName(CommandLine cmd) {
		if (cmd.hasOption("dir")) {
			this.dirName = cmd.getOptionValue("dir");
			log("dirName[" + this.dirName + "]");
		
			getFileNames();
		}
	}
	
	private void setDirName(String name){		
		this.dirName = name;
		log("dirName[" + this.dirName + "]");	
		getFileNames();
	}
	
	/**
	 * Gets the file names.
	 *
	 * @return the file names
	 */
	private void getFileNames() {
		File[] files = new File(dirName).listFiles();

		for (File file : files) {
		    if (file.isFile()) {
		    	String fileName = file.getName();		    	
		    	if(in(fileName)){
		    		fileNames.add(file.getName());
		    	}
		    }
		}
	}

	private boolean in(String fileName){
		for(String s : acceptedPrefix){
			if(fileName.toLowerCase().startsWith(s)){
				log("Found acceptable file ["+fileName+"]");
				return true;
			}
		}
		//log("Could not find ["+fileName+"] in accepted file prefix list, skipping file.");
		return false;
	}
	
	private void printHelp() {
	    HelpFormatter f = new HelpFormatter();
	    f.printHelp("OptionsTip", options);
	}
	
	private void log(String s){
		LOG.info(s);
	}
	
	/**
	 * Creates the options.
	 *
	 * @param args the args
	 * @return the command line
	 * @throws ParseException 
	 * @throws Exception the exception
	 */
	private CommandLine createOptions(String[] args) throws Exception {
		CommandLine cmd = null;
		options = new Options();

		OptionBuilder.withArgName("dir");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("dir name that hold muliple performance log files");
		options.addOption(OptionBuilder.create("dir"));

		OptionBuilder.withArgName("h");
		OptionBuilder.hasArg();
		OptionBuilder.withDescription("Print the help options");
		options.addOption(OptionBuilder.create("h"));

		CommandLineParser parser = new BasicParser();
		try {
			cmd = parser.parse(options, args);
		} catch (Exception e) {
			printHelp();
			throw e;
		}
		
		return cmd;
	}
	
	/**
	 * Adds the observer.
	 * 
	 * @param obs
	 *            the obs
	 */
	private void addObserver(Observer obs) {
		processObservers.add(obs);
	}

	public List<Observer> getObservers(){
		return processObservers;
	}
	
	/**
	 * Removes the observers.
	 */
	private void removeObservers() {
		for (Observer obs : processObservers) {
			obs.remove();
		}
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		TprController tprController = null;
		try {
			tprController = new TprController(args);
			
			// tprController.addObserver(new SystemOutProcessor());

			tprController.run();
			
		
			LOG.info("Complete.");

		} catch (Exception e) {
			e.printStackTrace();
			if( tprController != null) {
				tprController.printHelp();
			}
		}

	}

}
