package com.tangomc.tpr.data;

// TODO: Auto-generated Javadoc
/**
 * The Interface TprBaseData.
 */
public interface TprData {
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType();
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type);
	
	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public String getKey();
	
	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(String key);
	
	/**
	 * Gets the duration.
	 *
	 * @return the duration
	 */
	public long getDuration();
	
	/**
	 * Sets the duration.
	 *
	 * @param duration the new duration
	 */
	public void setDuration(long duration);
	
	/**
	 * Gets the start time.
	 *
	 * @return the start time
	 */
	public long getStartTime();
	
	/**
	 * Sets the start time.
	 *
	 * @param startTime the new start time
	 */
	public void setStartTime(long startTime);
	
	/**
	 * Gets the end time.
	 *
	 * @return the end time
	 */
	public long getEndTime();
	
	/**
	 * Sets the end time.
	 *
	 * @param endTime the new end time
	 */
	public void setEndTime(long endTime);
	
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public long getUserId();
	
	/**
	 * Sets the user id.
	 *
	 * @param userId the new user id
	 */
	public void setUserId(long userId);
	
	/**
	 * Gets the record id.
	 *
	 * @return the record id
	 */
	public long getRecordId();
	
	/**
	 * Sets the record id.
	 *
	 * @param recordId the new record id
	 */
	public void setRecordId(long recordId);
	
	/**
	 * Gets the source.
	 *
	 * @return the source
	 */
	public String getSource();
	
	/**
	 * Sets the source.
	 *
	 * @param source the new source
	 */
	public void setSource(String source);
}
