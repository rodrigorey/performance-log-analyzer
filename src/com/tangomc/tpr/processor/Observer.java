package com.tangomc.tpr.processor;

import java.util.Collection;

import com.tangomc.tpr.data.TprData;
import com.tangomc.tpr.processor.summary.SummaryData;

public interface Observer {

	public void process(TprData tprData);
	
	public void remove();
	
	public void store(String rootDir) throws Exception;

	public Collection<SummaryData> getSummary();
}
