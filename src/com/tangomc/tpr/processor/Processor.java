package com.tangomc.tpr.processor;

import java.util.ArrayList;

import com.tangomc.tpr.data.TprData;

public class Processor implements Subject {
	private ArrayList<Observer> observers = new ArrayList<Observer>();

	private static Processor instance = null;
	
	private Processor() {
		
	}

	 public static Processor getInstance() {
	      if(instance == null) {
	         instance = new Processor();
	      }
	      return instance;
	   }
	@Override
	public void registerObserver(Observer observer) {
		observers.add(observer);
		
	}

	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);
		
	}

	@Override
	public void notifyObservers(TprData tprData) {
		for(Observer obs : observers) {
			obs.process(tprData);
		}
		
	}

	@Override
	public void store(String rootDir) throws Exception {
		for(Observer obs : observers) {
			obs.store(rootDir);
		}
		
	}

}
