package com.tangomc.tpr.processor;

import com.tangomc.tpr.data.TprData;

public interface Subject {
	  public void registerObserver(Observer observer);

      public void removeObserver(Observer observer);

      public void notifyObservers(TprData tprData);
      
      public void store(String rootDir) throws Exception;

}
