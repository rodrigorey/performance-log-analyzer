package com.tangomc.tpr.processor.summary;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

// TODO: Auto-generated Javadoc
/**
 * The Class FileStore.
 */
public class FileStore implements SummaryStore {

	private static final Logger LOG = Logger.getLogger(FileStore.class);
	
	/** The output file name. */
	private String outputFileName = "temp.txt";
	
	/** The sum processors. */
	private final CellProcessor[] sum_processors = new CellProcessor[] {
			new NotNull(), // Type header
			new NotNull(), // Key number
			new ParseLong(), // MinVal 
			new ParseLong(), // MaxVal
			new ParseLong(), // total Count
			new ParseLong()}; // total Duration

	
	private final String[] header = new String[] { "type", "key", "minVal", "maxVal",
             "totalDuration", "totalCount" };
	/**
	 * Store.
	 * @throws Exception 
	 */
	public void store(Collection<SummaryData> col, String rootDir) throws Exception {
		LOG.info("FileStore...");
	    ICsvBeanWriter beanWriter = null;
	     
		try {
			File dir = new File(rootDir+File.separator+"output");
			dir.mkdirs();
			File file = new File(dir+File.separator+outputFileName);
			beanWriter = new CsvBeanWriter(new FileWriter(file), CsvPreference.TAB_PREFERENCE);
			
		    beanWriter.writeHeader(header);
		        
			// write the beans
            for( final SummaryData customer : col ) {
                    beanWriter.write(customer, header, sum_processors);
            }
		} finally {
            if( beanWriter != null ) {
                beanWriter.close();
            }
		}
	}
	
	@SuppressWarnings("unused")
	private void storeTprData(Collection<SummaryData> col) throws Exception {
		LOG.info("FileStore - TprData...");
	    ICsvBeanWriter beanWriter = null;	     
		try {
			beanWriter = new CsvBeanWriter(new FileWriter("tpr_"+outputFileName), CsvPreference.TAB_PREFERENCE);
			
		    beanWriter.writeHeader(header);
		    
			// write the beans
            for( final SummaryData customer : col ) {
                    beanWriter.write(customer, header, sum_processors);
            }
		} finally {
            if( beanWriter != null ) {
                beanWriter.close();
            }
		}
	}

	/**
	 * Gets the output file name.
	 *
	 * @return the output file name
	 */
	public String getOutputFileName() {
		return outputFileName;
	 
	}

	/**
	 * Sets the output file name.
	 *
	 * @param outputFileName the new output file name
	 */
	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}
	
	
}
