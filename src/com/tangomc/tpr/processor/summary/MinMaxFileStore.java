package com.tangomc.tpr.processor.summary;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

// TODO: Auto-generated Javadoc
/**
 * The Class MinMaxFileStore.
 */
public class MinMaxFileStore implements SummaryStore {

	private static final Logger LOG = Logger.getLogger(MinMaxFileStore.class);
	
	private static final String outputDir = "output";

	/** The output file name. */
	private String outputFileName = "temp.txt";
	
	/** The sum processors. */
	private final CellProcessor[] sum_processors = new CellProcessor[] {
			new NotNull(), // key
			new NotNull(), // type (Min or Max)
			new ParseLong(), // Val
			new NotNull()}; // Details

	/** The header. */
	private final String[] header = new String[] { "key", "type", "val",
    "details"};

	/* (non-Javadoc)
	 * @see com.tririga.custom.tpr.processor.summary.SummaryStore#store(java.util.Collection)
	 */
	public void store(Collection<SummaryData> col, String rootDir) throws Exception {
		LOG.info("FileStore - TprData...");
	    ICsvBeanWriter beanWriter = null;
	     
		try {
			File dir = new File(rootDir+File.separator+outputDir);
			dir.mkdirs();
			File file = new File(dir+File.separator+outputFileName);
			beanWriter = new CsvBeanWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE);
			
		    beanWriter.writeHeader(header);
		        
			// write the beans
            for( final SummaryData sumData : col ) {
                    beanWriter.write(getMinData(sumData), header, sum_processors);
                    beanWriter.write(getMaxData(sumData), header, sum_processors);
            }
            LOG.info("wrote to file ["+file.getAbsolutePath()+"]");
		} finally {
            if( beanWriter != null ) {
                beanWriter.close();
            }
		}
	}
	
	/**
	 * Gets the min data.
	 *
	 * @param data the data
	 * @return the min data
	 */
	private MinMaxData getMinData(SummaryData data) {
		MinData minData = new MinData();
				
		minData.setKey(data.getKey());
		minData.setType(data.getType());
		minData.setVal(data.getMinVal());
		minData.setDetails(data.getMinTprData().toString());
		return minData;
	}
	
	/**
	 * Gets the max data.
	 *
	 * @param data the data
	 * @return the max data
	 */
	private MinMaxData getMaxData(SummaryData data) {
		MaxData maxData = new MaxData();
		maxData.setKey(data.getKey());
		maxData.setType(data.getType());
		maxData.setVal(data.getMaxVal());
		maxData.setDetails(data.getMaxTprData().toString());
	
		return maxData;
	}

	/**
	 * Gets the output file name.
	 *
	 * @return the output file name
	 */
	public String getOutputFileName() {
		return outputFileName;
	}

	/**
	 * Sets the output file name.
	 *
	 * @param outputFileName the new output file name
	 */
	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}
	

}
