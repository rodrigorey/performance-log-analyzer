package com.tangomc.tpr.processor.summary;

import java.util.Collection;

import org.apache.log4j.Logger;

public class MinMaxOutStore implements SummaryStore {

	private static final Logger LOG = Logger.getLogger(MinMaxOutStore.class);
	
	@Override
	public void store(Collection<SummaryData> col, String rootDir) throws Exception {
		for(SummaryData sumData: col) {
			LOG.info("key="+sumData.getKey()+", Min="+sumData.getMinVal()+", " + sumData.getMinTprData());
			LOG.info("key="+sumData.getKey()+", Max="+sumData.getMaxVal()+", " + sumData.getMaxTprData());
		}
		
		
	}

}
