package com.tangomc.tpr.processor.summary;

import com.tangomc.tpr.data.TprData;

// TODO: Auto-generated Javadoc
/**
 * The Class SummaryData.
 */
public class SummaryData {

	/** The key. */
	private String key = "";
	
	/** The type. */
	private String type = "";
	
	
	/** The min val. */
	private long minVal = 0;
	
	/** The max val. */
	private long maxVal = 0;
		
	/** The total count. */
	private long totalCount = 0;
	
	/** The total duration. */
	private long totalDuration = 0;
	
	/** The min tpr data. */
	private TprData minTprData = null;
	
	/** The max tprdata. */
	private TprData maxTprdata = null;
	
	
	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	
	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	/**
	 * Gets the min val.
	 *
	 * @return the min val
	 */
	public long getMinVal() {
		return minVal;
	}
	
	/**
	 * Sets the min val.
	 *
	 * @param minVal the new min val
	 */
	public void setMinVal(long minVal) {
		this.minVal = minVal;
	}
	
	/**
	 * Gets the max val.
	 *
	 * @return the max val
	 */
	public long getMaxVal() {
		return maxVal;
	}
	
	/**
	 * Sets the max val.
	 *
	 * @param maxVal the new max val
	 */
	public void setMaxVal(long maxVal) {
		this.maxVal = maxVal;
	}
	
	/**
	 * Gets the avg val.
	 *
	 * @return the avg val
	 */
	public long getAvgVal() {
		return this.totalDuration/this.totalCount;
	}
	
	/**
	 * Gets the total count.
	 *
	 * @return the total count
	 */
	public long getTotalCount() {
		return totalCount;
	}
	
	/**
	 * Sets the total count.
	 *
	 * @param totalCount the new total count
	 */
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * Gets the total duration.
	 *
	 * @return the total duration
	 */
	public long getTotalDuration() {
		return totalDuration;
	}

	/**
	 * Sets the total duration.
	 *
	 * @param totalDuration the new total duration
	 */
	public void setTotalDuration(long totalDuration) {
		this.totalDuration = totalDuration;
	}

	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	
	/**
	 * Gets the min tpr data.
	 *
	 * @return the min tpr data
	 */
	public TprData getMinTprData() {
		return minTprData;
	}

	/**
	 * Sets the min tpr data.
	 *
	 * @param minTprData the new min tpr data
	 */
	public void setMinTprData(TprData minTprData) {
		this.minTprData = minTprData;
	}

	/**
	 * Gets the max tprdata.
	 *
	 * @return the max tprdata
	 */
	public TprData getMaxTprData() {
		return maxTprdata;
	}

	/**
	 * Sets the max tprdata.
	 *
	 * @param maxTprdata the new max tprdata
	 */
	public void setMaxTprdata(TprData maxTprdata) {
		this.maxTprdata = maxTprdata;
	}

	public static final String[] HEADER = new String[] { "type", "key", "minVal", "maxVal",
            "totalDuration", "totalCount" };
	
	@Override
	public String toString() {
		return "SummaryData [key=" + key + ", type=" + type + ", minVal="
				+ minVal + ", maxVal=" + maxVal + ", totalCount=" + totalCount
				+ ", totalDuration=" + totalDuration + ", minTprData="
				+ minTprData + ", maxTprdata=" + maxTprdata + "]";
	}



	
}
