package com.tangomc.tpr.processor.summary;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

// TODO: Auto-generated Javadoc
/**
 * The Class FileStore.
 */
public class SummaryFileStore implements SummaryStore {

	private static final Logger LOG = Logger.getLogger(SummaryFileStore.class);
	
	/** The output file name. */
	private String outputFileName = "temp.txt";
	private final String outputDir = "output";
	
	/** The sum processors. */
	private final CellProcessor[] sum_processors = new CellProcessor[] {
			new NotNull(), // Type header
			new NotNull(), // Key number
			new ParseLong(), // MinVal 
			new ParseLong(), // MaxVal
			new ParseLong(), // total Count
			new ParseLong()}; // total Duration

	/**
	 * Store.
	 * @throws Exception 
	 * @throws Exception 
	 */
	public void store(Collection<SummaryData> col, String rootDir) throws Exception {
		LOG.info("FileStore...");
	    ICsvBeanWriter beanWriter = null;
	     
	    
		try {
			//create the files and direct
			File dir = new File(rootDir+File.separator+outputDir);
			dir.mkdirs();			
			File file = new File(dir+File.separator+outputFileName);
			beanWriter = new CsvBeanWriter(new FileWriter(file), CsvPreference.STANDARD_PREFERENCE);
			
		    beanWriter.writeHeader(SummaryData.HEADER);
		        
			// write the beans
            for( final SummaryData customer : col ) {
                    beanWriter.write(customer, SummaryData.HEADER, sum_processors);
            }
            LOG.info("wrote to file ["+file.getAbsolutePath()+"]");
		} finally {
            if( beanWriter != null ) {
                beanWriter.close();
            }
		}
	}
	

	/**
	 * Gets the output file name.
	 *
	 * @return the output file name
	 */
	public String getOutputFileName() {
		return outputFileName;
	 
	}

	/**
	 * Sets the output file name.
	 *
	 * @param outputFileName the new output file name
	 */
	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}
	
	
}
