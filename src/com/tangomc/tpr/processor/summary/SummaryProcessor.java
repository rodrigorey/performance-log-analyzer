package com.tangomc.tpr.processor.summary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.tangomc.tpr.data.TprData;
import com.tangomc.tpr.processor.Observer;
import com.tangomc.tpr.processor.Processor;

// TODO: Auto-generated Javadoc
/**
 * The Class SummaryProcessor.
 */
public class SummaryProcessor implements Observer {

	/** The summary. */
	HashMap<String, SummaryData> summary = new HashMap<String, SummaryData>();
	
	List<SummaryStore> stores = new ArrayList<SummaryStore> ();
	
	/** The instance. */
	private Processor instance = Processor.getInstance();
	
	/**
	 * Instantiates a new summary processor.
	 */
	public SummaryProcessor() {
		instance.registerObserver(this);
	}
	
	/* (non-Javadoc)
	 * @see com.tririga.custom.tpr.processor.Observer#process(com.tririga.custom.tpr.data.TprData)
	 */
	@Override
	public void process(TprData tprData) {

		SummaryData sumData = summary.get(tprData.getKey());
		if( sumData == null) {
			sumData = setNewSumData(tprData);
		} else {
			sumData = this.updateSumData(sumData, tprData);
		}

		summary.put(sumData.getKey(), sumData);
		
	}
	
	/**
	 * Sets the new sum data.
	 *
	 * @param tprData the tpr data
	 * @return the summary data
	 */
	protected SummaryData setNewSumData(TprData tprData) {
		SummaryData sumData = new SummaryData();
		
		sumData.setKey(tprData.getKey());
		sumData.setType(tprData.getType());
		sumData.setTotalCount(1);
		sumData.setMaxVal(tprData.getDuration());
		sumData.setMinVal(tprData.getDuration());
		sumData.setTotalDuration(tprData.getDuration());
		sumData.setMaxTprdata(tprData);
		sumData.setMinTprData(tprData);
		
		return sumData;
	}

	/**
	 * Update sum data.
	 *
	 * @param sumData the sum data
	 * @param tprData the tpr data
	 * @return the summary data
	 */
	protected SummaryData updateSumData(SummaryData sumData, TprData tprData) {
		sumData.setTotalCount(sumData.getTotalCount()+1);
		
		if(sumData.getMaxVal() < tprData.getDuration()) {
			sumData.setMaxVal(tprData.getDuration()); 
			sumData.setMaxTprdata(tprData);

		}
		
		if( sumData.getMinVal() > tprData.getDuration()) {
			sumData.setMinVal(tprData.getDuration());
			sumData.setMinTprData(tprData);

		}
		
		sumData.setTotalDuration(sumData.getTotalDuration()+tprData.getDuration());
		
		return sumData;
	}
	
	/* (non-Javadoc)
	 * @see com.tririga.custom.tpr.processor.Observer#remove()
	 */
	@Override
	public void remove() {
		instance.removeObserver(this);
		
	}

	/*
	 * (non-Javadoc)
	 * @see com.tangomc.tpr.processor.Observer#getSummary()
	 */
	@Override
	public Collection<SummaryData> getSummary() {
		return summary.values();
	}
	/* (non-Javadoc)
	 * @see com.tririga.custom.tpr.processor.Observer#store()
	 */
	
	public void addStore(SummaryStore store) {
		stores.add(store);
	}
	
	@Override
	public void store(String rootDir) throws Exception {
		
		for(SummaryStore store : stores) {
			store.store(summary.values(), rootDir);
		}
				
	
	}

	
}
