package com.tangomc.tpr.processor.summary;

import java.util.Collection;

public interface SummaryStore {

	public void store(Collection<SummaryData> col, String rootDir) throws Exception;
	
}
