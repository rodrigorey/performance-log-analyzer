package com.tangomc.tpr.processor.summary;

import java.util.Collection;

import org.apache.log4j.Logger;

// TODO: Auto-generated Javadoc
/**
 * The Class FileStore.
 */
public class SystemOutStore implements SummaryStore {
	
	private static final Logger LOG = Logger.getLogger(SystemOutStore.class);
	
	/**
	 * Store.
	 */
	public void store(Collection<SummaryData> col, String rootDir) {
		
		for(SummaryData sumData: col) {
			LOG.info(sumData);
		}
		
	}

	
	
}
