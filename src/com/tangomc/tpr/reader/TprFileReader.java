package com.tangomc.tpr.reader;

import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

import com.tangomc.tpr.data.TprBaseData;
import com.tangomc.tpr.processor.Processor;
import com.tangomc.tpr.reader.mapper.TprMapper;

public class TprFileReader {

	private static final Logger LOG = Logger.getLogger(TprFileReader.class);
	
	private String fileName = "";

	private Processor proc = Processor.getInstance();

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void process() throws Exception {
		ICsvListReader listReader = null;
		try {
			listReader = new CsvListReader(new FileReader(fileName),
					CsvPreference.TAB_PREFERENCE);
			listReader.getHeader(false); // header can't be used with CsvListReader

			while ((listReader.read()) != null) {
				TprMapper mapper = TprMapperFactory.getTprMapper(listReader.length());
				final CellProcessor[] processors = mapper.getCellProcessor();
				TprBaseData tprData = mapper.map(listReader.executeProcessors(processors));
				proc.notifyObservers(tprData);
			}

		} catch (IOException e) {
			LOG.error(e,e);
		} finally {
			if (listReader != null) {
				listReader.close();
			}
		}

	}

	public void store(String rootDir) throws Exception {
		proc.store(rootDir);
	}
}
