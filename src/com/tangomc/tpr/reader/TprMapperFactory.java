package com.tangomc.tpr.reader;

import com.tangomc.tpr.reader.mapper.TprBaseMapper;
import com.tangomc.tpr.reader.mapper.TprBaseMapper20;
import com.tangomc.tpr.reader.mapper.TprExtFormulaMapper;
import com.tangomc.tpr.reader.mapper.TprMapper;
import com.tangomc.tpr.reader.mapper.TprReportMapper;
import com.tangomc.tpr.reader.mapper.TprWorkflowMapper;

public class TprMapperFactory {

	public static TprMapper getTprMapper(int columnSize) {
		TprMapper rtnMapper = null;
		
		switch(columnSize) {
			case TprBaseMapper.COLUMN_SIZE:
				rtnMapper = new TprBaseMapper();
				break;
			case TprExtFormulaMapper.COLUMN_SIZE:
				rtnMapper = new TprExtFormulaMapper();
				break;
			case TprReportMapper.COLUMN_SIZE:
				rtnMapper = new TprReportMapper();
				break;
			case TprWorkflowMapper.COLUMN_SIZE:
				rtnMapper = new TprWorkflowMapper();
				break;
			case TprBaseMapper20.COLUMN_SIZE:
				rtnMapper = new TprBaseMapper20();
				break;
			default:
				/// All mappers have this in common
				rtnMapper = new TprBaseMapper();
		}
		
		return rtnMapper;
	}
}
