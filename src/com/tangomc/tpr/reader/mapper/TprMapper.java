package com.tangomc.tpr.reader.mapper;

import java.util.List;

import org.supercsv.cellprocessor.ift.CellProcessor;

import com.tangomc.tpr.data.TprBaseData;

// TODO: Auto-generated Javadoc
/**
 * The Interface TprMapper.
 */
public interface TprMapper {
	/**
	 * Gets the cell processor.
	 * 
	 * @return the cell processor
	 */
	public CellProcessor[] getCellProcessor();

	/**
	 * Map.
	 *
	 * @param tprList the tpr list
	 * @return the tpr data
	 */
	public TprBaseData map(List<Object> tprList);


}
