package com.tangomc.tpr.reader.mapper;

import java.util.List;

import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ParseLong;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;

import com.tangomc.tpr.data.TprBaseData;
import com.tangomc.tpr.data.TprDataWorkflow;

// TODO: Auto-generated Javadoc
/**
 * The Class TprBaseMapper.
 */
public class TprWorkflowMapper implements TprMapper {

	/** The Constant COLUMN_SIZE. */
	public static final int COLUMN_SIZE = 26;
	
	/** The all processors. */
	private final CellProcessor[] processors = new CellProcessor[] {
			new NotNull(), // log4J header
			new NotNull(), // ignore number
			new NotNull(), // Type header
			new NotNull(), // Type
			new NotNull(), // Key header
			new NotNull(), // Key
			new NotNull(), // Duration header
			new ParseLong(), // Duration
			new NotNull(), // StartTime header
			new ParseLong(), // StartTime
			new NotNull(), // EndTime header
			new ParseLong(), // EndTime
			new NotNull(), // UserId header
			new ParseLong(), // UserId
			new NotNull(), // RecordId header
			new ParseLong(), // RecordId
			new NotNull(), // Source header
			new NotNull(), // Source
			new NotNull(), // Num Steps header
			new ParseLong(), // Num Steps
			new NotNull(), // WFId header
			new ParseLong(), // WFid
			new NotNull(), // WF Template Id header
			new ParseDouble(), // WF Template
			new NotNull(), // Info header
			new NotNull()}; // Info
			

	/**
	 * Gets the cell processor.
	 * 
	 * @return the cell processor
	 */
	public CellProcessor[] getCellProcessor() {
		return processors;
	}

	/* (non-Javadoc)
	 * @see com.tririga.custom.tpr.reader.mapper.TprMapper#map(java.util.List)
	 */
	public TprBaseData map(List<Object> tprList) {
		TprDataWorkflow rtnData = new TprDataWorkflow();
		TprMapperUtil util = new TprMapperUtil();
		
		//Map Base fields
		util.map(rtnData, tprList);
		
		//Map extended fields
		rtnData.setNumSteps((Long)tprList.get(19));
		rtnData.setWfId((Long)tprList.get(21));
		rtnData.setWfTempletId((Double)tprList.get(23));
		rtnData.setInfo((String) tprList.get(25));

		return rtnData;
	}
}
