/** 
 *
 */
package com.tririga.custom;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.tangomc.pla.BaseController;
import com.tangomc.pla.controllers.CleanLogs;
import com.tangomc.pla.controllers.GetResults;
import com.tangomc.pla.controllers.PerfLog;
import com.tangomc.pla.controllers.RunAutoAnalysis;
import com.tangomc.pla.utils.ResponseUtil;
import com.tririga.custom.integration.util.ReqUtils;
import com.tririga.custom.integration.util.ResponseUtils;
import com.tririga.pub.adapter.IConnect;
import com.tririga.ws.TririgaWS;

/**
 * Entry point from ServletProxy for Performance Log Analyzer.
 */
public class PLA implements IConnect {

	public static final String CLASSLOADER_NAME = "PLA";
	
	private Logger LOG = Logger.getLogger(PLA.class);
	
	/* (non-Javadoc)
	 * @see com.tririga.pub.adapter.IConnect#execute(com.tririga.ws.TririgaWS, javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void execute(TririgaWS client, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter(IConnect.PARAM_ACTION);
		String format = ReqUtils.getString(request.getParameter("f"), "json");
		StringBuilder sb = new StringBuilder();
		
		if(StringUtils.isNotBlank(action)){ 			
			try {
				BaseController service = ServiceList.getService(action);
				try{
					service.run(client, request, response);
				} catch(Exception e){
					LOG.error(e,e);
					JSONObject json = BaseController.handleError(e.getClass().getName()+" -> "+e.getMessage()+". Please double check that the request body is a valid JSON Object.");
					ResponseUtil.render(response, json, format);
				}									
				return;
			} catch (Exception e) {
				LOG.error(e, e);
				sb.append(e.getMessage());
				ResponseUtils.printPlainResponse(response, sb);	
			}
		} else {
			try {
				ResponseUtil.render(request, response, CLASSLOADER_NAME);
			} catch (Exception e) {
				LOG.error(e, e);
				sb.append(e.getMessage());
				ResponseUtils.printPlainResponse(response, sb);	
			}
			return;
		}				
	}
	
	public enum ServiceList{
		//add new enums for each process you want to analyze.
		//each new controller should extend BaseController
		PERF_LOG("perf", PerfLog.class), 
		GET_RESULTS("getResults", GetResults.class),
		CLEAN_LOGS("clean", CleanLogs.class),
		AUTO_ANALYSIS("auto", RunAutoAnalysis.class);
		
		private String actionType;
		private Class<?> service;
		
		/**
		 * pairs a service request action to a class
		 * @param actionType
		 * @param service
		 */
		ServiceList(String actionType, Class<?> service){
			this.actionType = actionType;
			this.service = service;
		}
				
		static BaseController getService(String actionType) throws InstantiationException, IllegalAccessException{
			for(ServiceList serviceList : ServiceList.values()){
				if(serviceList.actionType.equalsIgnoreCase(actionType)){
					return (BaseController) serviceList.service.newInstance();
				}
			}
			return null;
		}
		
		String getActionType(){
			return this.actionType;
		}
	}
}
