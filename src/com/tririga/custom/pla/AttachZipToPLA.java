/** 
 *
 */
package com.tririga.custom.pla;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.UUID;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import org.apache.log4j.Logger;

import com.tangomc.pla.PLAAbstract;
import com.tangomc.pla.client.Settings;
import com.tangomc.pla.services.ProcessLogs;
import com.tangomc.pla.utils.SystemUtils;
import com.tangomc.pla.utils.WFUtils;
import com.tririga.custom.integration.service.query.TririgaQuery;
import com.tririga.custom.integration.service.query.TririgaResultSet;
import com.tririga.custom.integration.service.query.TririgaRow;
import com.tririga.pub.workflow.CustomBusinessConnectTask;
import com.tririga.pub.workflow.Record;
import com.tririga.ws.TririgaWS;
import com.tririga.ws.dto.content.Content;
import com.tririga.ws.dto.content.InvalidContentException;
import com.tririga.ws.dto.content.Response;

/**
 * Called from the "tmcPerformance - send email" workflow custom task to grab the zip file 
 * generated for the performance log analyzer and attach it to the EMail Attachment record
 * passed in as the RecordId.
 */
public class AttachZipToPLA extends PLAAbstract implements CustomBusinessConnectTask {

	private static final Logger LOG = Logger.getLogger(AttachZipToPLA.class);

	/* (non-Javadoc)
	 * @see com.tririga.pub.workflow.CustomBusinessConnectTask#execute(com.tririga.ws.TririgaWS, long, com.tririga.pub.workflow.Record[])
	 */
	@Override
	public boolean execute(TririgaWS client, long userId, Record[] records) {
		try{
			this.client = client;
			this.client.register(userId);
			Long recordId = WFUtils.getRecordId(records);
			process(recordId);
			return true;
		} catch(Exception e){
			LOG.error(e,e);
			return false;
		}
	}


	public void process(Long recordId) throws Exception {
		Settings settings = getSettings(recordId);
		
		ProcessLogs pl = new ProcessLogs();
		String[] files = SystemUtils.getLinksToLatestFilesAsArray(settings.getLogPath());
		String fileNames = String.format("%s,%s", files[0], files[1]);
		File zip = pl.go(settings.getLogPath(), fileNames);

		//add zip to email object
		addZipToObject(recordId, zip);
		
		zip.delete();
		LOG.info("done.");
	}

	/**
	 * @param recordId
	 * @param zip
	 * @throws Exception 
	 * @throws FileNotFoundException 
	 */
	private void addZipToObject(Long recordId, File zip) throws FileNotFoundException, Exception {		
		Response response = saveBinary(recordId, "tmcPlaBI", new FileInputStream(zip));
		LOG.info("binary for ["+recordId+"] uploaded: "+response.getStatus());
	}
	
	private Response saveBinary(Long recordId, String fieldName, FileInputStream stream) throws Exception {
		Content content = new Content();
		if (null == recordId) {
			throw new InvalidContentException("Null recordId specified.");
		}
		if (null == stream) {
			throw new InvalidContentException(
					"An empty InputStream was attempted to be saved. ");
		}
		content.setRecordId(recordId);
		content.setFieldName(fieldName);
		content.setFileName("tmcPLA.zip");
		String random = UUID.randomUUID().toString();
		File temp = File.createTempFile(random, null);
		DataHandler dh = new DataHandler(new FileDataSource(temp));

		OutputStream out = new FileOutputStream(temp);
		int read = 0;
		byte[] bytes = new byte[1024];
		while ((read = stream.read(bytes)) != -1) {
			out.write(bytes, 0, read);
		}
		content.setContent(dh);
		Response response = this.client.upload(content);
		temp.delete();

		return response;
	}
	
}
