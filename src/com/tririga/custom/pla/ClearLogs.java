/** 
 *
 */
package com.tririga.custom.pla;

import org.apache.log4j.Logger;

import com.tangomc.pla.PLAAbstract;
import com.tangomc.pla.client.Settings;
import com.tangomc.pla.controllers.CleanLogs;
import com.tangomc.pla.utils.WFUtils;
import com.tririga.pub.workflow.CustomBusinessConnectTask;
import com.tririga.pub.workflow.Record;
import com.tririga.ws.TririgaWS;

/**
 * 
 */
public class ClearLogs extends PLAAbstract implements CustomBusinessConnectTask {

	private Logger LOG = Logger.getLogger(ClearLogs.class);
	
	/**
	 * 
	 */
	public ClearLogs() {
	}

	/* (non-Javadoc)
	 * @see com.tririga.pub.workflow.CustomBusinessConnectTask#execute(com.tririga.ws.TririgaWS, long, com.tririga.pub.workflow.Record[])
	 */
	@Override
	public boolean execute(TririgaWS client, long userId, Record[] records) {
		try {
			client.register(userId);
			this.client = client;
			
			process(WFUtils.getRecordId(records));
			return true;
			
		} catch (Exception e) {
			LOG.error(e, e);
			return false;
		}
	}

	/**
	 * @param recordId
	 * @throws Exception 
	 */
	@Override
	public void process(Long recordId) throws Exception {
		Settings settings = getSettings(recordId);
		CleanLogs cl = new CleanLogs();
		cl.clean(settings.getLogPath());
	}

}
