/** 
 *
 */
package com.tririga.custom.pla;

import java.util.List;

import org.apache.log4j.Logger;

import com.tangomc.pla.PLAAbstract;
import com.tangomc.pla.utils.WFUtils;
import com.tririga.custom.integration.service.query.TririgaQuery;
import com.tririga.custom.integration.service.query.TririgaResultSet;
import com.tririga.custom.integration.service.query.TririgaRow;
import com.tririga.custom.integration.service.query.TririgaRow.Data;
import com.tririga.pub.workflow.CustomBusinessConnectTask;
import com.tririga.pub.workflow.Record;
import com.tririga.ws.TririgaWS;

/**
 * Entry point from workflow for performance log reader analysis. 
 * Keep this class small.
 */
public class PLA extends PLAAbstract implements CustomBusinessConnectTask {
	
	private Logger LOG = Logger.getLogger(PLA.class);

	/* (non-Javadoc)
	 * @see com.tririga.pub.workflow.CustomBusinessConnectTask#execute(com.tririga.ws.TririgaWS, long, com.tririga.pub.workflow.Record[])
	 */
	@Override
	public boolean execute(TririgaWS client, long userId, Record[] records) {
		/*
		 * this operation should be handed the recordId for the meta data object 
		 * that defines what Service will be executed. 
		 */
		try{
			this.client = client;
			this.client.register(userId);
			process(WFUtils.getRecordId(records));			
			return true;			
		} catch(Exception e){
			LOG.error(e,e);
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.tangomc.pla.PLAAbstract#process(java.lang.Long)
	 */
	@Override
	public void process(Long recordId) throws Exception {
	}

	
	
}
