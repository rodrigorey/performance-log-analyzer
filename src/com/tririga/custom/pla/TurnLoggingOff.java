/** 
 *
 */
package com.tririga.custom.pla;

import org.apache.log4j.Logger;

import com.tangomc.pla.PLAAbstract;
import com.tangomc.pla.utils.WFUtils;
import com.tririga.pub.workflow.CustomBusinessConnectTask;
import com.tririga.pub.workflow.Record;
import com.tririga.ws.TririgaWS;

/**
 * 
 */
public class TurnLoggingOff extends PLAAbstract implements CustomBusinessConnectTask {

	public Logger LOG = Logger.getLogger(TurnLoggingOff.class);
	
	/**
	 * 
	 */
	public TurnLoggingOff() {
	}

	/* (non-Javadoc)
	 * @see com.tririga.pub.workflow.CustomBusinessConnectTask#execute(com.tririga.ws.TririgaWS, long, com.tririga.pub.workflow.Record[])
	 */
	@Override
	public boolean execute(TririgaWS client, long userId, Record[] records) {
		try {
			client.register(userId);
			this.client = client;
			
			process(WFUtils.getRecordId(records));
			return true;
			
		} catch (Exception e) {
			LOG.error(e, e);
			return false;
		}
	}
	
	/**
	 * @param recordId
	 */
	@Override
	public void process(Long recordId) {
		//nothing yet
	}
}