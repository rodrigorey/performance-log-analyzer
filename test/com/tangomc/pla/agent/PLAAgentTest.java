/** 
 *
 */
package com.tangomc.pla.agent;

import junit.framework.TestCase;

/**
 * 
 */
public class PLAAgentTest extends TestCase {

	public void testCheckLogs(){
		PLAgent agent = new PLAgent();		
		agent.checkLogs();
		assertTrue("agent logs not enabled, expected to be.", agent.logsEnabled);
	}
	
	public void testKillLogs(){
		PLAgent agent = new PLAgent();		
		agent.checkLogs();
		assertTrue("agent logs not enabled, expected to be.", agent.logsEnabled);
		
		agent.killLogs();
		assertFalse("agent logs still enabled, expected not to be.", agent.logsEnabled);
	}
}
