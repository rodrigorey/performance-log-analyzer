/** 
 *
 */
package com.tangomc.pla.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.mail.Address;
import javax.mail.internet.MimeMessage;

import junit.framework.TestCase;

/**
 * 
 */
public class ProcessLogsTest extends TestCase {
	
	public void testCollectJavaProperties() throws IOException{		
		ProcessLogs pl = new ProcessLogs();
		File f = pl.collectProperties("log");
		System.out.print(readFile(f));
		assertNotNull(f);
	}
	
	public void testCollectJavaPropertiesEmptyDir(){		
		ProcessLogs pl = new ProcessLogs();
		try{
			pl.collectProperties("");
			fail("expected exception for empty dir");
		} catch(Exception e){
			assertTrue(true);
		}
	}
	
	public void testCollectJavaPropertiesNoLogDir() throws IOException{		
		ProcessLogs pl = new ProcessLogs();
		File f = pl.collectProperties("papers");
		assertNotNull(f);
	}
	
	String readFile(File f) throws IOException {
	    BufferedReader br = new BufferedReader(new FileReader(f));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        return sb.toString();
	    } finally {
	        br.close();
	    }
	}
	
	public void testCreateNotificationForPerformance() throws Exception{
		ProcessLogs pl = new ProcessLogs();
		File zip = File.createTempFile("test", ".stuff");
		MimeMessage message = pl.createNotificationForPerformance(zip, "localhost");
		assertNotNull(message);
		Address[] recips = message.getAllRecipients();
		assertEquals(2, recips.length);
	}
}
