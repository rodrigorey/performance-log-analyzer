var BASE_URL = '../PLA';
var BASE_ADMIN_URL = '../../../admin/platformLogging.jsp';
$(function(){
	$('#dlBtn').hide();
	$('#cleanBtn').hide();
	$('#sendBtn').hide();
	$('#killBtn').hide();
	
	$('#runTestBtn').click(function(){
		analyze();
	});
	$('#magicBtn').click(function(){
		magic();
	});
	$('#cleanBtn').click(function(){
		removeOutputFiles();
	});
	$('#killBtn').click(function(){
		kill();
	});	
	$('#stopLogginBtn').click(function(){
		stopLogging();
	});
	
	checkStatus();
});	

function checkStatus(){
	showLoader();
	$.post(BASE_URL, {action:'auto', s:true}, function(data){				
	}).done(function(data){
		var status = "";
		try{data = $.parseJSON(data);} catch(error){}
		if(isNull(data.error)){
			status = data.status;
			if(isNull(status) || status.indexOf('NOT')>-1){
				$('#killBtn').hide();
				$('#magicBtn').show();
				$('#magicParams').show();
				$('#status').addClass('bad');
				$('#status').removeClass('good');
			} else {
				$('#killBtn').show();
				$('#magicBtn').hide();
				$('#magicParams').hide();
				$('#status').addClass('good');
				$('#status').removeClass('bad');
			}
		} else {
			status = data.error;
		}
		$('#status').text(status);
		hideLoader();
	}).error(function(data){
		showError(data);
	});
}
		
function stopLogging(){
	//turn off the logs first. We do it here, so the usersession can be reused.
	var params = {
		operation:"operation_update",
		operation_update: "Apply"
	};			

	$.post(BASE_ADMIN_URL, 
		params,	
		function(data){				
	}).done(function(data){
		showMsg("Success");
	}).error(function(data){
		showError(data.error);
	});			
}

function kill(){
	showLoader();
	stopLogging();
	killListener();
}

function killListener(){
	$.post(BASE_URL, {action:'auto', k:true}, function(data){				
	}).done(function(data){
		try{
			data = $.parseJSON(data);
		} catch(error){}
		if(isNull(data.error)){
			showMsg(data.status);	
		} else {
			showError(data.error);	
		}			
		checkStatus();
	}).error(function(data){
		showError(data.error);
	});			
}

function magic(){
	showLoader();
	var hours = $('#hoursToRun').val();
	if(isNull(hours) || isNaN(hours)){
		showError("Please specify a whole number, in hours, for how long to leave the performance logs on before analyzing.");
		return;
	}
	var dir = getLogDir();
	if(isNull(dir)){
		return;
	}
	
	//turn on the logs first. We do it here, so the usersession can be reused.
	$.post(BASE_ADMIN_URL, 
		$('#logform').serialize()	
		, function(data){				
	}).done(function(data){
		//showMsg(data);
		magicListener(dir, hours);
	}).error(function(data){
		showError(data.error);
	});
}

function magicListener(dir, hours){
	//setup the listener on the server to wait x hrs before anaylizing results and submitting.
	$.post(BASE_URL, {
		action:'auto',
		dir:dir,
		time:hours
	}, function(data){				
	}).done(function(data){
		try{data = $.parseJSON(data);}
		catch(error){}
		if(isNull(data.error)){
			showMsg('Auto analysis scheduled to run for '+hours+' hours.');	
		} else {
			showError(data.error);	
		}		
		checkStatus();
	}).error(function(data){
		showError(data);
	});
}

function removeOutputFiles(){
	if(confirm('Are you sure you want to delete the output files?')){				
		showLoader();
		var dir = getLogDir();
		if(isNull(dir)){
			return;
		}
		$.post(BASE_URL, {action:'clean', dir:dir}, function(data){					
		}).done(function(data){
			showMsg('Log files deleted successfully.');						
			$('#response').empty();
			$('#dlBtn').hide();
			$('#sendBtn').hide();
			$('#cleanBtn').hide();
		}).error(function(data){
			showError('error: '+data);					
		});
	}
}

function analyze(){
	showLoader();
	var dir = getLogDir();
	if(isNull(dir)){
		return;
	}
	$.post(BASE_URL, {action:'perf', dir:dir}, function(data){
	}).done(function(data){		
		try{data = $.parseJSON(data);}
		catch(error){}
		showSummary(data);
		hideLoader();
	}).error(function(data){					
		showError('error: '+data);				
	});
}

function getLogDir(){
	var dir = $('#baseUrl').val();
	if(isNull(dir)){
		showError('Please specify the root directory of where the logs to analyze are at.');
		return null;
	}
	return dir;
}

function showSummary(json){
	//clear the old stuff first
	$('#response').empty();
	$('#dlBtn').hide();
	$('#sendBtn').hide();
	
	var data = json.data;
	if(isNull(data)){
		if(!isNull(json.error)){
			showError(json.error);
		} else {
			showError("An unknown error has occurred. Double check your path to the server logs.");
		}
		return;
	}
	var table = $('<table></table>');
	var head = $('<thead></thead>');
	var body = $('<tbody></tbody>');
	
	var headers = json.header; //array of headers in order	        
	for (i=0;i<data.length;i++) {
		var row = $('<tr></tr>');
		//take care of headers first
        if(i==0){	            		            	
			for(h=0;h<headers.length;h++){
				var header = $('<th></th>');
				if(headers[h].indexOf('Val')>-1 || headers[h].indexOf('total')>-1){
					header.attr('data-sort', 'int');
					header.addClass('right');
				} else {
					header.attr('data-sort', 'string');
				}
				header.text(headers[h]);
				row.append(header);
			}
			head.append(row);
			table.append(head);					
			continue;
		} else {					
			for(col=0;col<headers.length;col++){
				var c = $('<td></td>');
				c.text(data[i][headers[col]]);
				row.append(c);
			}
		}				
        body.append(row);
    }
	table.append(body);			
    $('#response').append(table);
    var t = $(table).stupidtable();
    t.on("aftertablesort", function (event, data) {
        var th = $(this).find("th");
        th.find(".arrow").remove();
        var dir = $.fn.stupidtable.dir;
        var arrow = data.direction === dir.ASC ? " &uarr;" : " &darr;";
        th.eq(data.column).append('<span class="arrow">' + arrow +'</span>');
    });
    
    //add the download button
    $('#dlBtn').click(function(){
    	var dir = $('#baseUrl').val();
    	var files = json.links.minmax+","+json.links.summary; 
    	var url = BASE_URL+'?action=getResults&dir='+dir+'&files='+files+'&dl=true';
    	window.location.href = url;
    });
    $('#dlBtn').show();
    
  //add the download button
    $('#sendBtn').click(function(){
    	showLoader();
    	var dir = $('#baseUrl').val();
    	var files = json.links.minmax+","+json.links.summary; 
    	$.post(BASE_URL, {action:'getResults', dir:dir, files:files, dl:false}, function(data){
    	}).done(function(data){
    		try{data = $.parseJSON(data);}
    		catch(error){}
    		if(isNull(data.error)){
    			showMsg('Summary sent.');	
    		} else {
    			showError(data.error);	
    		}	        		
    	}).error(function(data){
			showError('error: '+data);					
    	});
    });
  $('#sendBtn').show();
    
    $('#cleanBtn').show();
}

function hideLoader(){
	$('#loader').hide();
}

function showLoader(){
	$('#loader').show();
}

function showMsg(s){
	$('#loader').hide();
	$('#msgGood').text(s);
	$('#msgGood').show();
	$('#msgGood').delay(3000).fadeOut();
}

function showError(s){
	$('#loader').hide();
	$('#msgBad').text(s);
	$('#msgBad').show();
	$('#msgBad').delay(3000).fadeOut();
}

function isNull(f){
	return (f==null || f===undefined || f==='undefined' || f.length==0 || f=='null')
}